package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;

import java.util.List;

public interface IProjectToTaskService {

    List<Task> findTaskByProjectId(String projectId);

    Task taskBindById(String projectId, String taskId);

    Task taskUnbindById(String projectId, String taskId);

    void removeAllTaskByProjectId(String projectId);

    Project removeById(String projectId);
}
