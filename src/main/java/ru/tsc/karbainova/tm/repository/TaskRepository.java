package ru.tsc.karbainova.tm.repository;

import ru.tsc.karbainova.tm.api.repository.ITaskRepository;
import ru.tsc.karbainova.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tascks = new ArrayList<>();

    @Override
    public void add(Task task) {
        tascks.add(task);
    }

    @Override
    public void remove(Task task) {
        tascks.remove(task);
    }

    @Override
    public boolean existsById(String id) {
        return findById(id) != null;
    }

    @Override
    public List<Task> findAll() {
        return tascks;
    }

    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> tasks = new ArrayList<>(tascks);
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public void clear() {
        tascks.clear();
    }

    @Override
    public Task findById(String id) {
        for (Task task : tascks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(int index) {
        if (tascks.size() == 0) return null;
        return tascks.get(index);
    }

    @Override
    public Task findByName(String name) {
        for (Task task : tascks) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeById(String id) {
        final Task task = findById(id);
        if (task == null) return null;
        tascks.remove(task);
        return task;
    }

    @Override
    public Task removeByName(String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        tascks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tascks.remove(task);
        return task;
    }

    @Override
    public Task taskUnbindById(String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(String projectId) {
        List<Task> listProject = findAllTaskByProjectId(projectId);
        for (Task task : listProject) {
            tascks.remove(task);
        }
    }

    @Override
    public Task bindTaskToProjectById(String projectId, String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(String projectId) {
        List<Task> listTasks = new ArrayList<>();
        for (Task task : tascks) {
            if (projectId.equals(task.getProjectId())) listTasks.add(task);
        }
        return listTasks;
    }
}
