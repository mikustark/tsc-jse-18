package ru.tsc.karbainova.tm.service;

import ru.tsc.karbainova.tm.api.repository.IProjectRepository;
import ru.tsc.karbainova.tm.api.repository.ITaskRepository;
import ru.tsc.karbainova.tm.api.service.IProjectToTaskService;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;

import java.util.List;

public class ProjectToTaskService implements IProjectToTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectToTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findTaskByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllTaskByProjectId(projectId);
    }

    @Override
    public Task taskBindById(String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existsById(projectId)) throw new EmptyIdException();
        if (!taskRepository.existsById(taskId)) throw new EmptyIdException();
        return taskRepository.bindTaskToProjectById(projectId, taskId);
    }

    @Override
    public Task taskUnbindById(String projectId, String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existsById(projectId)) throw new EmptyIdException();
        if (!taskRepository.existsById(taskId)) throw new EmptyIdException();
        return taskRepository.taskUnbindById(taskId);
    }

    @Override
    public void removeAllTaskByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        taskRepository.removeAllTaskByProjectId(projectId);
    }

    @Override
    public Project removeById(String projectId) {
        removeAllTaskByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }
}
