package ru.tsc.karbainova.tm.command.task;

import ru.tsc.karbainova.tm.command.ProjectAbstractCommand;
import ru.tsc.karbainova.tm.command.TaskAbstractCommand;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.util.TerminalUtil;

public class TaskFinishByIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "status-finish-by-id-task";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish by id";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(id);
        if (task == null) throw new TaskNotFoundException();
    }
}
