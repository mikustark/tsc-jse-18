package ru.tsc.karbainova.tm.command.project;

import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.ProjectAbstractCommand;
import ru.tsc.karbainova.tm.util.TerminalUtil;

public class ProjectCreateCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "create-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create project";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().create(name, description);
        System.out.println("[OK]");
    }
}
