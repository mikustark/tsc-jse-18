package ru.tsc.karbainova.tm.command.auth;

import ru.tsc.karbainova.tm.command.AuthAbstractCommand;
import ru.tsc.karbainova.tm.util.TerminalUtil;

public class AuthRegistryCommand extends AuthAbstractCommand {
    @Override
    public String name() {
        return "registry";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Registry app";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter email");
        final String email = TerminalUtil.nextLine();
        System.out.println("Enter password");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
    }
}
