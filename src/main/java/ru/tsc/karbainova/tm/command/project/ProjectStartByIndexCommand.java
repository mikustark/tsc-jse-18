package ru.tsc.karbainova.tm.command.project;

import ru.tsc.karbainova.tm.command.ProjectAbstractCommand;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.util.TerminalUtil;

public class ProjectStartByIndexCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "status-start-by-index-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start by index";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().startByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }
}
