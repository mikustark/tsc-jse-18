package ru.tsc.karbainova.tm.command.task;

import ru.tsc.karbainova.tm.command.TaskAbstractCommand;

public class TaskClearCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "clear-task";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Clear task";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        serviceLocator.getTaskService().clear();
        System.out.println("[OK]");
    }
}
